package fr.uavignon.ceri.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

import static fr.uavignon.ceri.tp1.data.Country.countries;

public class DetailFragment extends Fragment {

    TextView textTitle;
    TextView textDetail;
    TextView textLanguages;
    TextView textMoney;
    TextView textPopulation;
    TextView textArea;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textTitle = view.findViewById(R.id.item_title);
        textDetail = view.findViewById(R.id.item_detail);
        textLanguages = view.findViewById(R.id.item_languages);
        textMoney = view.findViewById(R.id.item_money);
        textPopulation = view.findViewById(R.id.item_population);
        textArea = view.findViewById(R.id.item_area);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int country = args.getCountryId();
        textTitle.setText(countries[country].getName());
        textDetail.setText(countries[country].getCapital());
        textLanguages.setText(countries[country].getLanguage());
        textMoney.setText(countries[country].getCurrency());
        textPopulation.setText(countries[country].getPopulation()+"");
        String area = countries[country].getArea() +" km2";
        textArea.setText(area);

        ImageView itemImage = view.findViewById(R.id.item_image);
        String uri = countries[country].getImgUri();
        Context c = view.getContext();
        int test = c.getResources().getIdentifier(uri, null, c.getPackageName());
        itemImage.setImageResource(test);


        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_ListFragment);
            }
        });
    }
}