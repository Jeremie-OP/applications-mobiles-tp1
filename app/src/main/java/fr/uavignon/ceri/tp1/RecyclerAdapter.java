package fr.uavignon.ceri.tp1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import static fr.uavignon.ceri.tp1.data.Country.countries;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {


    private String[] name = {"France","Allemagne","Japon","Afrique du Sud","Espagne","Etat-Unis"};

    private String[] detail = {"Paris","Berlin","Tokyo","Le Cap","Madrid","Washington"};

    private int[] images = { R.drawable.ic_flag_of_france_320px,
            R.drawable.ic_flag_of_germany_320px,
            R.drawable.ic_flag_of_japan_320px,
            R.drawable.ic_flag_of_south_africa_320px,
            R.drawable.ic_flag_of_spain_320px,
            R.drawable.ic_flag_of_the_united_states_320px};

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(countries[i].getName());
        viewHolder.itemDetail.setText(countries[i].getCapital());
        String uri = countries[i].getImgUri();
        Context c = viewHolder.itemView.getContext();
        int test = c.getResources().getIdentifier(uri, null, c.getPackageName());
        viewHolder.itemImage.setImageResource(test);
        //viewHolder.itemImage.setImageResource(images[i]);
    }

    @Override
    public int getItemCount() {
        return countries.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    //// Implementation with bundle
                    // Bundle bundle = new Bundle();
                    // bundle.putInt("numChapter", position);
                    // Navigation.findNavController(v).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);
                    ListFragmentDirections.ActionListFragmentToSecondFragment action = ListFragmentDirections.actionListFragmentToSecondFragment();
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });

        }
    }

}
